# top level makefile
.POSIX:
MAKE = make
SRC = src
MAN = man
LIB = lib

.PHONY: all $(LIB) $(SRC) \
	clean $(SRC)-clean $(LIB)-clean install

all: $(LIB) $(SRC)

clean: $(LIB)-clean $(SRC)-clean

$(SRC):
	$(MAKE) -C$@

$(SRC)-clean:
	$(MAKE) -C$(SRC) clean

$(LIB):
	$(MAKE) -C$@

$(LIB)-clean:
	$(MAKE) -C$(LIB) clean

install: all
	$(MAKE) -C$(SRC) install
	$(MAKE) -C$(MAN) install
