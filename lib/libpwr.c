/* 
 * libpwr.c -- GNU/Linux implementation of the core library
 *
 * Copyright (C) 2021 Aidan Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#define _GNU_SOURCE

#include "libpwr.h"

#include <dirent.h>
#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <err.h>

#define MAX_DEVICES 128
#define SYSFS_PATH "/sys/class/power_supply"

/* private function declarations */
PwrDevice *PwrDeviceNew(char *path);
void PwrDeviceReadSysfs(PwrDevice *device, char *var, char *varfmt, ...);
int PwrDeviceCheckSyfsExist(PwrDevice *device, char *var);

PwrDevice *PwrDeviceNew(char *path)
{
    PwrDevice *self = malloc(sizeof(PwrDevice));
    char *buf;

    self->name = strdup(path);
    self->free = &PwrDeviceFree;
    self->updateState = &PwrDeviceState;

    PwrDeviceReadSysfs(self, "type", "%ms", &buf);

    if (strcmp(buf, "Mains") == 0)
        self->type = PWR_DEVICE_ADAPTER;
    else if (strcmp(buf, "Battery") == 0)
        self->type = PWR_DEVICE_BATTERY;
    else if (strcmp(buf, "USB") == 0)
        self->type = PWR_DEVICE_USB;
    else
        self->type = PWR_DEVICE_OTHER;

    self->updateState(self);

    free(buf);

    return self;
}

void PwrDeviceFree(PwrDevice *self)
{
    free(self->name);
    free(self);
}

void PwrDeviceState(PwrDevice *self)
{
    if (self->type == PWR_DEVICE_ADAPTER) {
        int s;

        PwrDeviceReadSysfs(self, "online", "%i", &s);

        if (s == 1)
            self->state = PWR_ADAPTER_STATE_ONLINE;
        else if (s == 0)
            self->state = PWR_ADAPTER_STATE_OFFLINE;
        else
            self->state = PWR_ADAPTER_STATE_UNKNOWN;

    } else if (self->type == PWR_DEVICE_BATTERY) {
        long energy_now, energy_full;
        char *status;

        if (PwrDeviceCheckSyfsExist(self, "energy_now"))
            PwrDeviceReadSysfs(self, "energy_now", "%li", &energy_now);
        else if (PwrDeviceCheckSyfsExist(self, "charge_now"))
            PwrDeviceReadSysfs(self, "charge_now", "%li", &energy_now);
        else
            errx(1, "%s: Cannot find properties energy_now or charge_now",
                 self->name);


        if (PwrDeviceCheckSyfsExist(self, "energy_full"))
            PwrDeviceReadSysfs(self, "energy_full", "%li", &energy_full);
        else if (PwrDeviceCheckSyfsExist(self, "charge_full"))
            PwrDeviceReadSysfs(self, "charge_full", "%li", &energy_full);
        else
            errx(1, "%s: Cannot find properties energy_full or charge_full",
                 self->name);

        self->charge = ((double) energy_now / (double) energy_full) * 100;

        PwrDeviceReadSysfs(self, "status", "%ms", &status);
        if (strncmp(status, "Discharging", strlen("Discharging")) == 0)
            self->state = PWR_BATTERY_STATE_DISCHARGING;
        else if (strncmp(status, "Charging", strlen("Charging")) == 0)
            self->state = PWR_BATTERY_STATE_CHARGING;
        else if (strncmp(status, "Not Charging", strlen("Not Charging")) == 0)
            self->state = PWR_BATTERY_STATE_NOT_CHARGING;
        else if (strncmp(status, "Full", strlen("Full")) == 0)
            self->state = PWR_BATTERY_STATE_FULL;
        else
            self->state = PWR_BATTERY_STATE_UNKNOWN;

        free(status);
    }
}

// utility function for reading from sysfs
void PwrDeviceReadSysfs(PwrDevice *device, char *var, char *varfmt, ...)
{
    va_list args;
    FILE *f;
    char *pth;

    if ((asprintf(&pth, SYSFS_PATH "/%s/%s", device->name, var)) < 0)
        err(errno, "asprintf");

    if ((f = fopen(pth, "r")) == NULL)
        err(errno, "%s", pth);

    va_start(args, varfmt);
    if (vfscanf(f, varfmt, args) == EOF)
        err(errno, "%s", pth);

    va_end(args);

    fclose(f);
    free(pth);
}

// utility function for checking if property exists
int PwrDeviceCheckSyfsExist(PwrDevice *device, char *var)
{
    char *pth;
    int r;

    if ((asprintf(&pth, SYSFS_PATH "/%s/%s", device->name, var)) < 0)
        err(errno, "asprintf");

    r = access(pth, F_OK);
    free(pth);

    return r != -1;
}

int enumeratePwrDevices(PwrDevice ***targ)
{
    PwrDevice **devices;
    struct dirent *diritem;
    DIR *dir;
    int devcnt;

    devices = calloc(MAX_DEVICES, sizeof(PwrDevice *));
    if (devices == NULL)
        err(errno, "calloc");

    if ((dir = opendir(SYSFS_PATH)) == NULL)
        err(errno, SYSFS_PATH);

    devcnt = 0;
    while (((diritem = readdir(dir)) != NULL) && devcnt <= MAX_DEVICES) {
        // exclude '.' and '..' entries
        if (strncmp(diritem->d_name, ".", 1) != 0) {
            devices[devcnt] = PwrDeviceNew(diritem->d_name);
            devcnt++;
        } else {
            continue;
        }
    }

    closedir(dir);
    *targ = devices;

    return devcnt;
}
