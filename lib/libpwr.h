/*
 * libpwr.h -- common functions
 *
 * Copyright (C) 2021 Aidan Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBPWR_H
#define LIBPWR_H

enum PwrDeviceType {
    PWR_DEVICE_ADAPTER,
    PWR_DEVICE_BATTERY,
    PWR_DEVICE_USB,
    PWR_DEVICE_OTHER
};

enum PwrDeviceState {
    PWR_ADAPTER_STATE_ONLINE,
    PWR_ADAPTER_STATE_OFFLINE,
    PWR_ADAPTER_STATE_UNKNOWN,
    PWR_BATTERY_STATE_CHARGING,
    PWR_BATTERY_STATE_DISCHARGING,
    PWR_BATTERY_STATE_NOT_CHARGING,
    PWR_BATTERY_STATE_FULL,
    PWR_BATTERY_STATE_UNKNOWN
};

typedef struct PwrDevice PwrDevice;
struct PwrDevice {
    char *name;
    enum PwrDeviceType type;
    enum PwrDeviceState state;
    double charge;

    void (*free)(PwrDevice *);
    void (*updateState)(PwrDevice *);
};

void PwrDeviceState(PwrDevice *self);
void PwrDeviceFree(PwrDevice *self);

// devices must be NULL
int enumeratePwrDevices(PwrDevice ***devices);

#endif
