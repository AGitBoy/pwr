# pwr
## simple set of CLI laptop power programs

This project consists of four simple command line programs which will output
power information for laptops.

### Programs included
| Program | Description                    |
|---------|--------------------------------|
| adpt    | Prints adapter status          |
| chrg    | Prints battery charging status |
| lsbat   | Lists power devices and info   |
| pwr     | Prints the charge              |

All programs have a man page included.

### Install
Run the following command in the cloned directory
```shell
sudo make install
```

### License
Copyright (C) 2021 Aidan Williams

pwr is free software. See the file COPYING for copying conditions.
