/*
 * pwr.c -- print battery charge
 *
 * Copyright (C) 2021 Aidan Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <libpwr.h>
#include <err.h>

#ifdef __GNUC__
#define ESCP '\e'
#else
#define ESCP 033
#endif

void usage()
{
    fprintf(stderr, "usage: pwr [-b battery] [-f format]\n");
    exit(1);
}

void printe(char *str, double arg)
{
    int c;

    while ((c = *str++) != '\0') {
        if (c != '\\') {
            printf("%c", c);
            continue;
        }

        c = *str++;

        if (c == '\0') {
            printf("%c", c);
            exit(0);
        }

        switch (c) {
            case 'a':
                printf("\a");
                break;
            case 'b':
                printf("\b");
                break;
            case 'e':
                printf("%c", 033);
                break;
            case 'f':
                printf("\f");
                break;
            case 'n':
                printf("\n");
                break;
            case 'r':
                printf("\r");
                break;
            case 't':
                printf("\t");
                break;
            case 'v':
                printf("\v");
                break;
            case 's':
                printf("%i", (int) round(arg));
                break;
            case 'd':
                printf("%.2f", arg);
                break;
            default:
                printf("\\%c", c);
                break;
        }
    }
}

// get power from battery or average of all batteries
double pwr(char *device)
{
    PwrDevice **devices;
    int devcnt, i;
    double r = -1;
    double batavg = 0.0;
    int batcnt = 0;

    devcnt = enumeratePwrDevices(&devices);

    for (i = 0; i < devcnt; i++) {
        if (devices[i]->type == PWR_DEVICE_BATTERY) {
            if (device == NULL) {
                batavg += devices[i]->charge;
                batcnt++;
            } else
                if (strncmp(devices[i]->name, device, strlen(devices[i]->name))
                    == 0) {
                r = devices[i]->charge;
                break;
            }
        }
    }

    if (device == NULL) {
        if (batcnt > 0)
            r = batavg / batcnt;
        else
            errx(1, "no batteries found");
    } else if (r < 0) {
        errx(1, "%s: no such battery found", device);
    }

    for (i = 0; i < devcnt; i++)
        devices[i]->free(devices[i]);

    free(devices);

    return r;
}

int main(int argc, char **argv)
{
    char *dev = NULL;
    char *fmt = "\\s\n";
    int o;

    while ((o = getopt(argc, argv, ":b:f:")) != -1) {
        switch (o) {
            case 'b':
                dev = optarg;
                break;
            case 'f':
                fmt = optarg;
                break;
            default:
                usage();
        }
    }

    if (optind < argc)
        usage();

    printe(fmt, pwr(dev));

    return 0;
}
