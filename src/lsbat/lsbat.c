/*
 * lsbat.c -- lists power devices on system
 *
 * Copyright (C) 2021 Aidan Williams
 *
 * pwr is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pwr is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pwr.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <libpwr.h>

void usage()
{
    fprintf(stderr, "usage: lsbat [-t]\n");
    exit(1);
}

void printdev(PwrDevice *device, int tabulate)
{
    if (device->type == PWR_DEVICE_BATTERY) {
        int charge = (int) round(device->charge);

        if (tabulate)
            printf("%s\tBattery\t%i\n", device->name, charge);
        else
            printf("%s (Battery): %i%%\n", device->name, charge);
    } else if (device->type == PWR_DEVICE_ADAPTER) {
        char *state;
        if (device->state == PWR_ADAPTER_STATE_ONLINE)
            state = "Plugged in";
        else if (device->state == PWR_ADAPTER_STATE_OFFLINE)
            state = "Unplugged";
        else
            state = "Unknown";

        if (tabulate)
            printf("%s\tAdapter\t%s\n", device->name, state);
        else
            printf("%s (Adapter): %s\n", device->name, state);
    } else if (device->type == PWR_DEVICE_USB) {
        if (tabulate)
            printf("%s\tUSB\n", device->name);
        else
            printf("%s (USB)\n", device->name);
    } else {
        if (tabulate)
            printf("%s\tOther\n", device->name);
        else
            printf("%s (Other)\n", device->name);
    }

}

int main(int argc, char **argv)
{
    PwrDevice **devices;
    int devcnt, i, o;
    int tflag = 0;

    while ((o = getopt(argc, argv, "t")) != -1) {
        switch (o) {
            case 't':
                tflag = 1;
                break;
            default:
                usage();
        }
    }

    devcnt = enumeratePwrDevices(&devices);

    for (i = 0; i < devcnt; i++)
        printdev(devices[i], tflag);

    for (i = 0; i < devcnt; i++)
        devices[i]->free(devices[i]);

    free(devices);

    return 0;
}
