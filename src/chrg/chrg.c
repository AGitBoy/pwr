/*
 * chrg.c -- print charging status
 *
 * Copyright (C) 2021 Aidan Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <err.h>
#include <libpwr.h>

#define CHRGSTR "Charging"
#define DCHRGSTR "Discharging"
#define FULLSTR "Full"
#define NCHRGSTR "Not Charging"
#define UNKNOWNSTR "Unknown"

char *chrgstr = CHRGSTR;
char *dchrgstr = DCHRGSTR;
char *fullstr = FULLSTR;
char *nchrgstr = NCHRGSTR;
char *unknownstr = UNKNOWNSTR;

void usage()
{
    fprintf(stderr, "usage: chrg [-c charging] [-d discharging]"
            " [-f full] [-n not] [-u unknown]\n");
    exit(1);
}

int main(int argc, char **argv)
{
    PwrDevice **devices;
    int devcnt, i, o;
    int foundbat;

    while ((o = getopt(argc, argv, ":c:d:f:n:u:")) != -1) {
        switch (o) {
            case 'c':
                chrgstr = optarg;
                break;
            case 'd':
                dchrgstr = optarg;
                break;
            case 'f':
                fullstr = optarg;
                break;
            case 'n':
                nchrgstr = optarg;
                break;
            case 'u':
                unknownstr = optarg;
                break;
            default:
                usage();
        }
    }

    devcnt = enumeratePwrDevices(&devices);

    foundbat = 0;
    for (i = 0; i < devcnt; i++) {
        if (devices[i]->type == PWR_DEVICE_BATTERY) {
            switch (devices[i]->state) {
                case PWR_BATTERY_STATE_CHARGING:
                    puts(chrgstr);
                    break;
                case PWR_BATTERY_STATE_DISCHARGING:
                    puts(dchrgstr);
                    break;
                case PWR_BATTERY_STATE_FULL:
                    puts(fullstr);
                    break;
                case PWR_BATTERY_STATE_NOT_CHARGING:
                    puts(nchrgstr);
                    break;
                default:
                    puts(unknownstr);
                    break;
            }

            foundbat = 1;
            break;
        }
    }

    for (i = 0; i < devcnt; i++)
        devices[i]->free(devices[i]);

    free(devices);

    if (!foundbat) {
        fprintf(stderr, "chrg: no battery found\n");
        return 1;
    }

    return 0;
}
