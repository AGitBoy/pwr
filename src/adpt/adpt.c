/*
 * adpt.c -- print adapter status
 *
 * Copyright (C) 2021 Aidan Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <libpwr.h>

char *onstr = "Plugged in";
char *offstr = "Unplugged";
char *unknownstr = "Unkown";

void usage()
{
    fprintf(stderr, "usage: adpt [-o on] [-f off] [-u unknown]\n");
    exit(1);
}

int main(int argc, char **argv)
{
    PwrDevice **devices;
    int devcnt, i, o;
    int foundadpt;

    while ((o = getopt(argc, argv, ":o:f:u:")) != -1) {
        switch (o) {
            case 'o':
                onstr = optarg;
                break;
            case 'f':
                offstr = optarg;
                break;
            case 'u':
                unknownstr = optarg;
                break;
            default:
                usage();
        }
    }

    devcnt = enumeratePwrDevices(&devices);

    foundadpt = 0;
    for (i = 0; i < devcnt; i++) {
        if (devices[i]->type == PWR_DEVICE_ADAPTER) {
            switch (devices[i]->state) {
                case PWR_ADAPTER_STATE_ONLINE:
                    puts(onstr);
                    break;
                case PWR_ADAPTER_STATE_OFFLINE:
                    puts(offstr);
                    break;
                default:
                    puts(unknownstr);
                    break;
            }

            foundadpt = 1;
            break;
        }
    }

    for (i = 0; i < devcnt; i++)
        devices[i]->free(devices[i]);

    free(devices);

    if (!foundadpt) {
        fprintf(stderr, "adpt: no adapter found\n");
        return 1;
    }

    return 0;
}
